��          �       �      �  
   �  	   �     �     �     �     �  E   �  W   9     �     �  
   �     �     �     �     �     �     �  F        H     _  \   o  =   �     
  �             +  -   ?  0   m     �     �  >   �  }     -   �     �  (   �      �          #  (   5     ^     u  p   �  0   �      /  �   P     �     �   % Comments 1 Comment Comment navigation Comments are closed. Continue reading %s Edit It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Next Next Image Nothing Found Pages: Previous Previous Image Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Used between list items, there is a space after the comma.,  the WordPress team PO-Revision-Date: 2014-12-04 10:48:59+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Twenty Fifteen
 % Коментари  1 Коментар  Навигация в коментарите  Коментарите са затворени.  Нататък %s  Редактиране  Тук няма нищо. Опитайте с търсене?  Страницата не съществува. Пробвайте да потърсите каквото ви трябва.  Публикуване на коментар  Следваща  Следващо изображение  Нищо не е открито  Страници:  Предишна  Предишно изображение  Главно меню  Създаден с %s  Време е за първата ви публикация. <a href="%1$s">Започнете от тук</a>.  Резултати от търсене за: %s  Към съдържанието  Няма резултати, отговарящи на вашето търсене. Опитайте отново с други ключови думи.   ,   Екипът на WordPress   